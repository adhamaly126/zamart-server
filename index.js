require('dotenv/config');

const express = require('express');
const app = express();
const cron = require('node-cron');
const mongooseLoader = require('./loaders/mongoose-loader');

require('./loaders/index')(app);
mongooseLoader();

const ItemService = require('./src/item/item-service');
const itemService = new ItemService();

//Check every day at 11:55 pm
cron.schedule('55 23 * * *', async function () {
  console.log('Item Schedule Running ...');
  await itemService.checkExpiryDateForExpiredItems();
  console.log('Item Schedule IS Scheduled ...');
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.info(`Listening on PORT ${PORT}`);
});
