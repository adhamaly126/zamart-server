require('make-promises-safe');
require('express-async-errors');

const compression = require('compression');
const helmet = require('helmet');
const express = require('express');
const cors = require('cors');
const AbstractError = require('../src/common/errors/abstract-error');
const routerv1 = require('../routes/v1');

function shouldCompress(req, res) {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false;
  }

  // fallback to standard filter function
  return compression.filter(req, res);
}

module.exports = (app) => {
  app.use(compression({ filter: shouldCompress }));
  app.use(helmet());
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  app.use(
    cors({
      origin: '*',
    }),
  );

  app.use('/api', routerv1);

  // eslint-disable-next-line no-unused-vars
  app.use((err, req, res, next) => {
    // res.locals.message = typeof err === 'object' ? err.message : err
    console.error(err);

    if (err instanceof AbstractError) {
      return res
        .status(err.statusCode)
        .send({ success: false, errors: err.serializeErrors().flat() });
    }

    res.status(500).send({
      success: false,
      errors: [
        {
          message: 'Something went wrong',
        },
      ],
    });
  });

  // eslint-disable-next-line no-unused-vars
  app.use((req, res, next) => {
    res.status(404).send({
      success: false,
      errors: [
        {
          message: 'NotFound: there is no handler for this url',
        },
      ],
    });
  });
};
