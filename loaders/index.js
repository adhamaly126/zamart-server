const expressLoader = require('./express-loader');

module.exports = (app) => {
  expressLoader(app);
};
