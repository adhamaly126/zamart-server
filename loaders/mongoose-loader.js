const mongoose = require('mongoose');
const AdminService = require('../src/admin/admin-service');
const adminService = new AdminService();

module.exports = () => {
  const DB = process.env.DB_CONNECTION;
  mongoose.set('strictQuery', true);
  mongoose
    .connect(DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .then(async () => {
      console.info('Connected to MongoDB...');
      await adminService.injectAdmin();
    })
    .catch((err) => console.error(`Couldn't connect to MongoDB... `, err));
};
