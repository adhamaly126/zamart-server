class MessageFactory {
  constructor() {}

  static itemAddedByOwnerToAdminMessage(adminId, itemId) {
    return {
      ar: {
        title: '',
        body: '',
      },
      en: {
        title: 'test',
        body: 'Check new added ad for item',
      },
      data: {
        receiver: adminId.toString(),
        receiverModel: 'admin',
        clickableItem: itemId.toString(),
        clickableItemModel: 'item',
      },
    };
  }

  static itemPublishedByAdminMessage(userId, itemId) {
    return {
      ar: {
        title: '',
        body: '',
      },
      en: {
        title: 'test',
        body: 'Item Published Success..',
      },
      data: {
        receiver: userId.toString(),
        receiverModel: 'user',
        clickableItem: itemId.toString(),
        clickableItemModel: 'item',
      },
    };
  }

  static itemRejectedByAdminMessage(userId, itemId) {
    return {
      ar: {
        title: '',
        body: '',
      },
      en: {
        title: 'test',
        body: 'Item Rejected By Admin ',
      },
      data: {
        receiver: userId.toString(),
        receiverModel: 'user',
        clickableItem: itemId.toString(),
        clickableItemModel: 'item',
      },
    };
  }
}

module.exports = MessageFactory;
