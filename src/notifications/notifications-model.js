const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotificationSchema = new Schema(
  {
    receiver: { type: Schema.Types.ObjectId, refPath: 'receiverModel' },

    receiverModel: {
      type: String,
      enum: ['user', 'admin'],
    },

    clickableItem: {
      type: Schema.Types.ObjectId,
      refPath: 'clickableItemModel',
    },

    clickableItemModel: {
      type: String,
      enum: ['item'],
    },

    message: {
      arTitle: { type: String },
      arBody: { type: String },
      enTitle: { type: String },
      enBody: { type: String },
    },

    isRead: { type: Boolean, default: false },
  },
  { timestamps: true },
);

const NotificationModel = mongoose.model('notifications', NotificationSchema);
module.exports = NotificationModel;
