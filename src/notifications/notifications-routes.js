const notificationsRoutes = require('express').Router();

const {
  isUserAuthenticated,
} = require('../common/middlewares/auth-middlewares');

const NotificationService = require('./notifications-service');
const notificationService = new NotificationService();

notificationsRoutes.get('', isUserAuthenticated, async (req, res) => {
  const { page, perPage } = req.query;
  const notificationsPagination =
    await notificationService.getNotificationByReceiverId(
      req.user.id,
      page,
      perPage,
    );
  res.status(200).json({
    success: true,
    totalPages: notificationsPagination.totalPages,
    unReadCount: notificationsPagination.unReadNotificationsCount,
    totalCount: notificationsPagination.count,
    data: notificationsPagination.notifications,
  });
});
notificationsRoutes.patch('/:notificationId/set-read', async (req, res) => {
  res.status(200).json({
    success: true,
    data: await notificationService.setRead(req.params.notificationId),
  });
});

module.exports = notificationsRoutes;
