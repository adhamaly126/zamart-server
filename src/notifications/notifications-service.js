const FCMService = require('../common/services/fcm-service');
const PaginationService = require('../common/services/pagination-service');
const MessageFactory = require('./messages/messages-factory');
const NotificationModel = require('./notifications-model');

class NotificationService {
  constructor() {}

  async sendOwnerAddNewItemNotification(itemId, adminId) {
    const sendOwnerAddNewItemMessage =
      MessageFactory.itemAddedByOwnerToAdminMessage(adminId, itemId);

    await this._saveNotification(sendOwnerAddNewItemMessage);
  }

  async itemPublishedByAdminNotification(
    userId,
    itemId,
    userLanguage,
    userFcmToken,
  ) {
    const itemPublishedByAdminMessage =
      MessageFactory.itemPublishedByAdminMessage(userId, itemId);

    if (userFcmToken)
      await FCMService.pushNotificationToDevice({
        notification:
          itemPublishedByAdminMessage[userLanguage] ||
          itemPublishedByAdminMessage['en'],
        data: itemPublishedByAdminMessage.data,
        token: userFcmToken,
      });

    await this._saveNotification(itemPublishedByAdminMessage);
  }

  async itemRejectedByAdminNotification(
    userId,
    itemId,
    userLanguage,
    userFcmToken,
  ) {
    const itemRejectedByAdminMessage =
      MessageFactory.itemRejectedByAdminMessage(userId, itemId);

    if (userFcmToken)
      await FCMService.pushNotificationToDevice({
        notification:
          itemRejectedByAdminMessage[userLanguage] ||
          itemRejectedByAdminMessage['en'],
        data: itemRejectedByAdminMessage.data,
        token: userFcmToken,
      });

    await this._saveNotification(itemRejectedByAdminMessage);
  }

  async _saveNotification(message) {
    await NotificationModel.create({
      receiver: message.data.receiver,
      receiverModel: message.data.receiverModel,
      clickableItem: message.data.clickableItem,
      clickableItemModel: message.data.clickableItemModel,
      'message.arTitle': message.ar.title,
      'message.arBody': message.ar.body,
      'message.enTitle': message.en.title,
      'message.enBody': message.en.body,
    });
  }

  async getNotificationByReceiverId(receiverId, page = 1, perPage = 10) {
    const { skip, limit } = PaginationService.getSkipAndLimit(
      parseInt(page),
      parseInt(perPage),
    );

    const notifications = await NotificationModel.find({
      receiver: receiverId,
    })
      .sort({ createdAt: -1 })
      .skip(skip)
      .limit(limit);

    const count = await NotificationModel.countDocuments({
      receiver: receiverId,
    });

    const totalPages = PaginationService.getTotalPages(count, limit);

    const unReadNotificationsCount = await NotificationModel.countDocuments({
      receiver: receiverId,
      isRead: false,
    });

    return { notifications, totalPages, count, unReadNotificationsCount };
  }

  async setRead(notificationId) {
    await NotificationModel.findByIdAndUpdate(notificationId, {
      $set: { isRead: true },
    });
  }
}
module.exports = NotificationService;
