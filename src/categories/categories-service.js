const { default: mongoose } = require('mongoose');
const CategoriesModel = require('./models/category-model');
const ComplementaryCategoriesModel = require('./models/complementary-category-model');
const SubCategoriesModel = require('./models/sub-category-model');
const CategoryFormDataSModel = require('./models/category-form-data-mode');
const MethodNotAllowedException = require('../common/errors/method-not-allowed-exception');
const fsPromises = require('fs').promises;
class CategoriesService {
  constructor() {}

  async createCategeory(data, image) {
    await CategoriesModel.create({
      ...data,
      image: image,
    });
  }

  async createCategorySubCategories(categorySubCategories) {
    categorySubCategories.subCategories?.forEach(async (subCategory) => {
      return await SubCategoriesModel.create({
        ...subCategory,
        category: categorySubCategories.category,
      });
    });
  }

  async createSubCategoryComplementaries(subCategoryComplementaries) {
    subCategoryComplementaries.complementaryCategories?.forEach(
      async (complementary) => {
        return await ComplementaryCategoriesModel.create({
          ...complementary,
          subCategory: subCategoryComplementaries.subCategory,
        });
      },
    );
  }
  async findAllCategories() {
    const categories = await CategoriesModel.find().exec();

    return this._categoriesImagesFormater(categories);
  }
  async findCategoryById(id) {
    return await CategoriesModel.findById(id).exec();
  }
  async findAllSubCategories(categoryId) {
    const subCategoriesByCategory = categoryId
      ? { category: mongoose.Types.ObjectId(categoryId) }
      : {};

    const subCategories = await SubCategoriesModel.aggregate([
      {
        $match: { ...subCategoriesByCategory },
      },
      {
        $lookup: {
          from: 'complementary-categories',
          localField: '_id',
          foreignField: 'subCategory',
          as: 'complementaryCategories',
        },
      },
    ]).exec();

    return subCategories;
  }
  async findSubCategoryById(id) {
    return await SubCategoriesModel.findById(id).exec();
  }
  async findAllComplementaryCategories(subCategoryId) {
    const subCategoryExist = subCategoryId
      ? { subCategory: subCategoryId }
      : {};

    return await ComplementaryCategoriesModel.find(subCategoryExist).exec();
  }
  async findComplementaryCategoryById(id) {
    return await ComplementaryCategoriesModel.findById(id).exec();
  }

  async createCategoryFormData(data) {
    const { formDataFields, category } = data;

    const isCategoryHasForm = await CategoryFormDataSModel.findOne({
      category: category,
    }).exec();
    if (isCategoryHasForm)
      throw new MethodNotAllowedException('Category Already Has Form Data');

    await CategoryFormDataSModel.create({
      formDataFields,
      category: category,
    });
  }

  async updateCategoryFormData(data, categoryId) {
    const { formDataFields } = data;

    if (!formDataFields?.length)
      throw new MethodNotAllowedException(
        'Category must have at least one custom field',
      );

    await CategoryFormDataSModel.updateOne(
      { category: categoryId },
      {
        $set: {
          formDataFields,
        },
      },
    ).exec();
  }

  async getCategoryFormData(category) {
    const categoryCustomFields = await CategoryFormDataSModel.findOne(
      { category: category },
      { formDataFields: 1, createdAt: 1 },
    ).exec();

    return categoryCustomFields ? categoryCustomFields['formDataFields'] : [];
  }

  async getAllSystemFields() {
    const fields = await fsPromises
      .readFile(__dirname + '/systemFields.json', 'utf-8')
      .catch((err) => console.error('Failed to read System Fields', err));

    return JSON.parse(fields);
  }

  _categoriesImagesFormater(categories) {
    const convertedCategories = categories?.map((category) => {
      return {
        ...category._doc,
        image: Buffer.from(category['image'], 'binary').toString('base64'),
      };
    });

    return convertedCategories;
  }
}

module.exports = CategoriesService;
