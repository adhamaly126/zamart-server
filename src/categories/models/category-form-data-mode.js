const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategoryFormDataSchema = new Schema(
  {
    category: { type: Schema.Types.ObjectId, ref: 'category' },
    formDataFields: [{ type: Schema.Types.Mixed }],
  },
  { timestamps: true },
);
const CategoryFormDataSModel = mongoose.model(
  'category-form',
  CategoryFormDataSchema,
);
module.exports = CategoryFormDataSModel;
