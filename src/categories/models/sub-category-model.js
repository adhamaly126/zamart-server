const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SubCategoriesSchema = new Schema(
  {
    nameAr: { type: String },
    nameEn: { type: String },
    category: { type: Schema.Types.ObjectId, ref: "category" }
  },
  { timestamps: true }
);
const SubCategoriesModel = mongoose.model("sub-category", SubCategoriesSchema);
module.exports = SubCategoriesModel;
