const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategoriesSchema = new Schema(
  {
    nameAr: { type: String },
    nameEn: { type: String },
    country: { type: Schema.Types.ObjectId, ref: 'country' },
    description: { type: String },
    image: { type: Buffer },
  },
  { timestamps: true },
);
const CategoriesModel = mongoose.model('category', CategoriesSchema);
module.exports = CategoriesModel;
