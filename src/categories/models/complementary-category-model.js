const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ComplementaryCategoriesSchema = new Schema(
  {
    nameAr: { type: String },
    nameEn: { type: String },
    subCategory: { type: Schema.Types.ObjectId, ref: 'sub-category' },
  },
  { timestamps: true },
);
const ComplementaryCategoriesModel = mongoose.model(
  'complementary-category',
  ComplementaryCategoriesSchema,
);
module.exports = ComplementaryCategoriesModel;
