const { check } = require('express-validator');
const {
  handleRouteValidationError,
} = require('../common/errors/handle-validation-error');

const categoryIdValidator = [
  check('categoryId')
    .notEmpty()
    .withMessage('Enter categoryId ')
    .bail()
    .isMongoId()
    .withMessage('Invalid Id'),
  handleRouteValidationError,
];

const subCategoryIdValidator = [
  check('subCategoryId')
    .notEmpty()
    .withMessage('Enter subCategoryId ')
    .bail()
    .isMongoId()
    .withMessage('Invalid Id'),
  handleRouteValidationError,
];

const categoryIdValidator2 = [
  check('category')
    .notEmpty()
    .withMessage('Enter category ')
    .bail()
    .isMongoId()
    .withMessage('Invalid Id'),
  handleRouteValidationError,
];

module.exports = {
  categoryIdValidator,
  subCategoryIdValidator,
  categoryIdValidator2,
};
