const categoriesRoutes = require('express').Router();

const multer = require('multer');
const fs = require('fs/promises');
const path = require('path');
const sharp = require('sharp');

const {
  isGuestOrAuthenticatedUser,
  isUserAuthenticated,
  isAdmin,
} = require('../common/middlewares/auth-middlewares');
const CategoriesService = require('./categories-service');
const {
  categoryIdValidator,
  categoryIdValidator2,
  subCategoryIdValidator,
} = require('./category-validation');
const categoriesService = new CategoriesService();

const storage = multer.diskStorage({
  destination: `uploads`,
  filename(req, file, cb) {
    cb(null, file.fieldname + '-' + file.originalname + '-' + Date.now());
  },
  filetype(req, file, cb) {
    cb(null, path.extname(file.originalname));
  },
});

// const fileFilter = (req, file, cb) => {
//   if (
//     file.mimetype === 'image/jpeg' ||
//     file.mimetype === 'image/jpg' ||
//     file.mimetype === 'image/png'
//   ) {
//     cb(null, true);
//   } else {
//     cb(null, false);
//   }
// };

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 10 * 1024 * 1024,
  },
});
const uploadFile = upload.single('image');
const uploadImage = (req, res, next) => {
  uploadFile(req, res, (err) => {
    if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
      if (err.code === 'LIMIT_FILE_SIZE') {
        return res.status(405).json({
          message: 'File size is too large',
        });
      } else {
        console.log(err);
        return res.status(405).json({
          message: 'Failed while uploading image',
        });
      }
    }

    // Everything is ok.
    next();
  });
};

categoriesRoutes.post('/', isUserAuthenticated, isAdmin, uploadImage, create);
categoriesRoutes.post(
  '/form-data',
  isUserAuthenticated,
  isAdmin,
  createCategoryFormData,
);
categoriesRoutes.put(
  '/form-data',
  isUserAuthenticated,
  isAdmin,
  updateCategoryFormData,
);

categoriesRoutes.post(
  '/sub-categories',
  isUserAuthenticated,
  isAdmin,
  createCategorySubCategories,
);
categoriesRoutes.post(
  '/sub-categories/complementary-categories',
  isUserAuthenticated,
  isAdmin,
  createSubCategoryComplementaries,
);

categoriesRoutes.get('/all', isGuestOrAuthenticatedUser, getAllCategories);
categoriesRoutes.get('/fields', isGuestOrAuthenticatedUser, getAllFields);

categoriesRoutes.get(
  '/:categoryId',
  isGuestOrAuthenticatedUser,
  getCategoryById,
);

categoriesRoutes.get(
  '/:categoryId/form-data',
  categoryIdValidator,
  isGuestOrAuthenticatedUser,
  getCategoryFormData,
);

categoriesRoutes.get(
  '/sub-categories/all',
  categoryIdValidator2,
  isGuestOrAuthenticatedUser,
  getAllSubCategories,
);
categoriesRoutes.get(
  '/sub-categories/:subCategoryId',
  isGuestOrAuthenticatedUser,
  getSubCategoryById,
);
categoriesRoutes.get(
  '/complementary-categories/all',
  subCategoryIdValidator,
  isGuestOrAuthenticatedUser,
  getAllComplementaryCategories,
);
categoriesRoutes.get(
  '/complementary-categories/:complementaryCategoryId',
  isGuestOrAuthenticatedUser,
  getComplementaryCategoryById,
);

async function getAllCategories(req, res) {
  res.status(200).json({
    success: true,
    data: await categoriesService.findAllCategories(),
  });
}
async function getCategoryById(req, res) {
  res.status(200).json({
    success: true,
    data: await categoriesService.findCategoryById(req.params.categoryId),
  });
}
async function getAllSubCategories(req, res) {
  const { category } = req.query;

  res.status(200).json({
    success: true,
    data: await categoriesService.findAllSubCategories(category),
  });
}

async function getSubCategoryById(req, res) {
  res.status(200).json({
    success: true,
    data: await categoriesService.findSubCategoryById(req.params.subCategoryId),
  });
}

async function getAllComplementaryCategories(req, res) {
  const { subCategory } = req.query;

  res.status(200).json({
    success: true,
    data: await categoriesService.findAllComplementaryCategories(subCategory),
  });
}

async function getComplementaryCategoryById(req, res) {
  res.status(200).json({
    success: true,
    data: await categoriesService.findComplementaryCategoryById(
      req.params.complementaryCategoryId,
    ),
  });
}

async function create(req, res) {
  console.log(req.file);
  const bufferdFile = await _filesSharping(req.file);
  await categoriesService.createCategeory(req.body, bufferdFile);
  res.status(201).json({
    success: true,
  });
}

async function createCategorySubCategories(req, res) {
  await categoriesService.createCategorySubCategories(req.body);
  res.status(201).json({
    success: true,
  });
}

async function createSubCategoryComplementaries(req, res) {
  await categoriesService.createSubCategoryComplementaries(req.body);
  res.status(201).json({
    success: true,
  });
}

async function createCategoryFormData(req, res) {
  await categoriesService.createCategoryFormData(req.body);
  res.status(201).json({
    success: true,
  });
}

async function getCategoryFormData(req, res) {
  res.status(201).json({
    success: true,
    data: await categoriesService.getCategoryFormData(req.params.categoryId),
  });
}

async function getAllFields(req, res) {
  res.status(201).json({
    success: true,
    data: await categoriesService.getAllSystemFields(),
  });
}

async function updateCategoryFormData(req, res) {
  await categoriesService.updateCategoryFormData(
    req.body,
    req.query.categoryId,
  );
  res.status(201).json({
    success: true,
  });
}

async function _filesSharping(file) {
  const bufferedImage = await sharp(file.path)
    .resize({
      width: 97,
      height: 97,
    })
    .webp({ lossless: true })
    .toBuffer();

  await fs.unlink(process.cwd() + '/uploads/' + file.filename);

  return bufferedImage;
}
module.exports = categoriesRoutes;
