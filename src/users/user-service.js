const ForbiddenException = require('../common/errors/forbidden-exception');
const MethodNotAllowedException = require('../common/errors/method-not-allowed-exception');
const NotFoundException = require('../common/errors/not-found-exception');
const EmailService = require('../common/services/emails-service');
const UserModel = require('./user-model');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const ItemsModel = require('../item/models/item-model');
const ImagesModel = require('../item/models/image-model');
const Base64Formater = require('../common/services/base64Formater');
const PaginationService = require('../common/services/pagination-service');

class UserService {
  constructor() {}

  async registeration(registerationData) {
    const emailExist = await UserModel.findOne({
      email: registerationData.email,
      isDeleted: false,
    }).exec();

    if (emailExist)
      throw new MethodNotAllowedException('Email is already registered');

    const hashedPassword = await bcrypt.hash(
      registerationData.password,
      parseInt(process.env.SALT),
    );

    const user = await UserModel.create({
      fullName: registerationData.fullName,
      email: registerationData.email,
      phone: registerationData.phone,
      password: hashedPassword,
      fcmToken: registerationData.fcmToken
        ? registerationData.fcmToken
        : undefined,
    });

    // Generate token
    const token = jwt.sign(
      { email: registerationData.email },
      process.env.EMAIL_VERIFICATION_SECRET,
    );

    // Send email verification
    EmailService.sendEmail(registerationData.email, token, 'VERIFICATION');

    return user;
  }
  async login(body) {
    const { email, password, fcmToken } = body;

    const user = await UserModel.findOne({ email: email, isDeleted: false })
      .select('+password')
      .exec();

    if (!user) throw new NotFoundException('Email is not exist');

    if (!user.isVerified)
      throw new MethodNotAllowedException('Verify You Account');

    const isPasswordMatches = await bcrypt.compare(password, user.password);

    if (!isPasswordMatches)
      throw new MethodNotAllowedException('Invalid user credentials');

    if (fcmToken) {
      user.fcmToken = fcmToken;
      await user.save();
    }
    return user;
  }
  async logout(userId) {
    const user = await UserModel.findOne({
      _id: userId,
      isDeleted: false,
    }).exec();

    if (!user) throw new NotFoundException('User Account Not Found');

    user.fcmToken = undefined;
    await user.save();
  }

  async findUserProfileById(id) {
    const user = await UserModel.findOne({ _id: id, isDeleted: false }).exec();
    if (!user) throw new NotFoundException('User Account Not Found');

    user.profileViews += 1;
    await user.save();

    const mostViewedItem = await ItemsModel.findOne({ user: id })
      .sort({
        totalViews: -1,
      })
      .populate();

    let image;
    if (mostViewedItem)
      image = await ImagesModel.findOne({ item: mostViewedItem._id });

    return {
      user,
      mostViewedItem: mostViewedItem ? mostViewedItem._doc : {},
      image: image ? Base64Formater.parseToBase64(image['img']) : {},
    };
  }

  async findMyProfile(id) {
    const user = await UserModel.findOne({ _id: id, isDeleted: false }).exec();
    if (!user) throw new NotFoundException('User Account Not Found');

    return user;
  }

  async updateUserLangugae(userId, language) {
    const user = await UserModel.findOne({
      _id: userId,
      isDeleted: false,
    }).exec();

    if (!user) throw new NotFoundException('User Account Not Found');

    user.langugae = language;
    await user.save();
  }

  async deleteMyAccount(userId) {
    const user = await UserModel.findOne({
      _id: userId,
      isDeleted: false,
    }).exec();

    if (!user) throw new NotFoundException('User Account Not Found');

    await UserModel.updateOne(
      { _id: userId, isDeleted: false },
      { $set: { isDeleted: true } },
    );
  }

  async activateAccount(token) {
    let payload;
    try {
      // Decode token
      payload = jwt.verify(token, process.env.EMAIL_VERIFICATION_SECRET);
    } catch (error) {
      throw new ForbiddenException('Forbidden Access');
    }
    // Verify email
    await UserModel.updateOne(
      { email: payload.email, isDeleted: false },
      { $set: { isVerified: true } },
    ).exec();

    return 'SUCCESS';
  }

  forgetPassword(email) {
    // Generate token
    const token = jwt.sign(
      { email: email },
      process.env.RESET_PASSWORD_SECRETS,
    );

    // Send email
    EmailService.sendEmail(email, token, 'RESET_PASSWORD');
  }

  async resetPassword(token, newPassword) {
    let payload;
    try {
      // Decode token
      payload = jwt.verify(token, process.env.RESET_PASSWORD_SECRETS);
    } catch (error) {
      throw new ForbiddenException('Forbidden Access');
    }

    const hashedPassword = await bcrypt.hash(
      newPassword,
      parseInt(process.env.SALT),
    );

    await UserModel.updateOne(
      { email: payload.email, isDeleted: false },
      { $set: { password: hashedPassword } },
    ).exec();
  }
  async getAllUsers(query) {
    const { skip, limit } = PaginationService.getSkipAndLimit(
      parseInt(query.page) || 1,
      parseInt(query.perPage) || 10,
    );

    const users = await UserModel.find({ isDeleted: false })
      .sort({ createdAt: -1 })
      .skip(skip)
      .limit(limit);

    const count = await UserModel.countDocuments({ isDeleted: false });

    return {
      users,
      totalPages: PaginationService.getTotalPages(count, limit),
      totalItems: count,
    };
  }
}
module.exports = UserService;
