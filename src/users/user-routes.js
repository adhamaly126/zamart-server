const userRoutes = require('express').Router();

const {
  isUserAuthenticated,
  isGuestOrAuthenticatedUser,
  isAdmin,
} = require('../common/middlewares/auth-middlewares');
const {
  registeragionController,
  loginController,
  activateController,
  refreshTokenController,
  forgetPasswordController,
  resetPasswordController,
  findMyProfileById,
  findUserProfileById,
  logoutController,
  deleteMyAccountController,
  updateUserLanguageController,
  getAllUsersByAdminController,
} = require('./user-controllers');

userRoutes.post('/sign-up', registeragionController);
userRoutes.post('/sign-in', loginController);
userRoutes.post('/refresh-token', refreshTokenController);
userRoutes.post('/forget-password', forgetPasswordController);
userRoutes.post('/reset-password', resetPasswordController);
userRoutes.get('/activate', activateController);
userRoutes.post('/logout', isUserAuthenticated, logoutController);
userRoutes.get('/my-profile', isUserAuthenticated, findMyProfileById);
userRoutes.get(
  '/all',
  isUserAuthenticated,
  isAdmin,
  getAllUsersByAdminController,
);
userRoutes.delete('/account', isUserAuthenticated, deleteMyAccountController);
userRoutes.patch(
  '/update-language',
  isUserAuthenticated,
  updateUserLanguageController,
);

userRoutes.get(
  '/seller-profile/:userId',
  isGuestOrAuthenticatedUser,
  findUserProfileById,
);

module.exports = userRoutes;
