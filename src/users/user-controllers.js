const ForbiddenException = require('../common/errors/forbidden-exception');
const JwtService = require('../common/services/jwt-service');
const UserService = require('./user-service');
const userService = new UserService();
const jwt = require('jsonwebtoken');

async function registeragionController(req, res) {
  const user = await userService.registeration(req.body);
  // Generates Tokens
  const { accessToken, refreshToken } = JwtService.generateTokens({
    id: user._id,
    role: 'USER',
  });

  res.status(200).json({
    success: true,
    data: { ...user._doc, accessToken, refreshToken, password: undefined },
  });
}

async function loginController(req, res) {
  const user = await userService.login(req.body);

  // Generates Tokens
  const { accessToken, refreshToken } = JwtService.generateTokens({
    id: user._id,
    role: 'USER',
  });

  res.status(200).json({
    success: true,
    data: { ...user._doc, accessToken, refreshToken, password: undefined },
  });
}

async function logoutController(req, res) {
  res.status(200).json({
    success: true,
    data: await userService.logout(req.user.id),
  });
}

async function deleteMyAccountController(req, res) {
  res.status(200).json({
    success: true,
    data: await userService.deleteMyAccount(req.user.id),
  });
}

async function updateUserLanguageController(req, res) {
  res.status(200).json({
    success: true,
    data: await userService.updateUserLangugae(req.user.id, req.body.language),
  });
}

async function activateController(req, res) {
  const { token } = req.query;
  const result = await userService.activateAccount(token);

  res.send(
    result === 'SUCCESS'
      ? '<html><h1> Verification success </h1></html>'
      : '<html><h1>Verification failed</h1></html>',
  );
}

function forgetPasswordController(req, res) {
  const { email } = req.body;

  userService.forgetPassword(email);

  res.status(200).json({ success: true });
}
async function resetPasswordController(req, res) {
  const { token, newPassword } = req.body;

  await userService.resetPassword(token, newPassword);

  res.status(200).json({ success: true });
}

function refreshTokenController(req, res) {
  const { expiredRefreshToken } = req.body;

  let decodedPayload;
  // Verify
  try {
    decodedPayload = jwt.verify(
      expiredRefreshToken,
      process.env.REFRESH_TOKEN_SECRETS,
    );
  } catch (error) {
    throw new ForbiddenException('Forbidden Access');
  }

  console.log(decodedPayload);
  // Generates Tokens
  const { accessToken, refreshToken } = JwtService.generateTokens({
    id: decodedPayload.id,
    role: decodedPayload.role,
  });

  res.status(200).json({
    success: true,
    data: { accessToken, refreshToken },
  });
}

async function findMyProfileById(req, res) {
  res.status(200).json({
    success: true,
    data: await userService.findMyProfile(req.user.id),
  });
}

async function findUserProfileById(req, res) {
  const { user, mostViewedItem, image } = await userService.findUserProfileById(
    req.params.userId,
  );
  res.status(200).json({
    success: true,
    data: {
      ...user._doc,
      mostViewedItem: { ...mostViewedItem, image },
    },
  });
}

async function getAllUsersByAdminController(req, res) {
  const result = await userService.getAllUsers(req.query);

  res.status(200).json({
    success: true,
    totalPages: result.totalPages,
    totalItems: result.totalItems,
    data: result.users,
  });
}
module.exports = {
  registeragionController,
  loginController,
  activateController,
  refreshTokenController,
  forgetPasswordController,
  resetPasswordController,
  findMyProfileById,
  findUserProfileById,
  logoutController,
  deleteMyAccountController,
  updateUserLanguageController,
  getAllUsersByAdminController,
};
