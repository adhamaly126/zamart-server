// Import mongoose
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema(
  {
    fullName: { type: String },
    email: { type: String },
    phone: { type: String },
    password: { type: String, select: false },
    imageLink: { type: String },
    imagePath: { type: String },
    isVerified: { type: Boolean, default: false },
    isDeleted: { type: Boolean, default: false },
    profileViews: { type: Number, default: 0 },
    fcmToken: { type: String },
    langugae: { type: String, default: 'en' },
  },
  {
    timestamps: true,
  },
);

const UserModel = mongoose.model('user', UserSchema);
module.exports = UserModel;
