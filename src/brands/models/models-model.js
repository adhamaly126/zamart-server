const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ModelSchema = new Schema(
  {
    brand: { type: Schema.Types.ObjectId, ref: 'brand' },
    name: { type: String },
  },
  { timestamps: true },
);
const ModelMongooseModel = mongoose.model('model', ModelSchema);
module.exports = ModelMongooseModel;
