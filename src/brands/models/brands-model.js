const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BrandsSchema = new Schema(
  {
    category: { type: Schema.Types.ObjectId, ref: 'category' },
    name: { type: String },
  },
  { timestamps: true },
);
const BrandsModel = mongoose.model('brand', BrandsSchema);
module.exports = BrandsModel;
