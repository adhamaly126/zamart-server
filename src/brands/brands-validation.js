const { check } = require('express-validator');
const {
  handleRouteValidationError,
} = require('../common/errors/handle-validation-error');

const brandIdValidator = [
  check('brandId')
    .notEmpty()
    .withMessage('Enter brandId ')
    .bail()
    .isMongoId()
    .withMessage('Invalid Id'),
  handleRouteValidationError,
];

module.exports = {
  brandIdValidator,
};
