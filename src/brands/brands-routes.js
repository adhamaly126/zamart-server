const BrandsRoutes = require('express').Router();
const {
  isUserAuthenticated,
  isAdmin,
} = require('../common/middlewares/auth-middlewares');
const BrandsService = require('./brands-service');
const { brandIdValidator } = require('./brands-validation');
const brandService = new BrandsService();

BrandsRoutes.post('/', isUserAuthenticated, isAdmin, async (req, res) => {
  const { categoryId, names } = req.body;
  await brandService.insertBrands(categoryId, names);

  res.status(200).json({
    success: true,
  });
});

BrandsRoutes.get('/', async (req, res) => {
  const { categoryId } = req.query;

  res.status(200).json({
    success: true,
    data: await brandService.getBrands(categoryId),
  });
});

BrandsRoutes.post(
  '/:brandId/models',
  isUserAuthenticated,
  isAdmin,
  async (req, res) => {
    const { names } = req.body;

    await brandService.insertModels(req.params.brandId, names);

    res.status(200).json({
      success: true,
    });
  },
);

BrandsRoutes.get('/:brandId/models', brandIdValidator, async (req, res) => {
  res.status(200).json({
    success: true,
    data: await brandService.getModels(req.params.brandId),
  });
});

module.exports = BrandsRoutes;
