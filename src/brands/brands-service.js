const BrandsModel = require('./models/brands-model');
const ModelMongooseModel = require('./models/models-model');

class BrandsService {
  constructor() {}

  async insertBrands(categoryId, names) {
    names.forEach(async (name) => {
      await BrandsModel.create({ category: categoryId, name: name });
    });
  }

  async getBrands(categoryId) {
    return await BrandsModel.find({
      ...(categoryId ? { category: categoryId } : {}),
    });
  }

  async insertModels(brandId, names) {
    names.forEach(async (name) => {
      await ModelMongooseModel.create({ brand: brandId, name: name });
    });
  }

  async getModels(brandId) {
    return await ModelMongooseModel.find({ brand: brandId });
  }
}

module.exports = BrandsService;
