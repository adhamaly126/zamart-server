const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReportsSchema = new Schema(
  {
    item: { type: mongoose.Schema.Types.ObjectId, ref: 'item' },
    reason: { type: String },
  },
  { timestamps: true },
);

const ReportsModel = mongoose.model('report', ReportsSchema);
module.exports = ReportsModel;
