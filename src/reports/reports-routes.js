const ReportsRoutes = require('express').Router();
const {
  isUserAuthenticated,
  isAdmin,
  isGuestOrAuthenticatedUser,
} = require('../common/middlewares/auth-middlewares');
const ReportsModel = require('./reports-model');

ReportsRoutes.post('/', isGuestOrAuthenticatedUser, async (req, res) => {
  await ReportsModel.create(req.body);
  res.status(200).json({
    success: true,
  });
});

ReportsRoutes.get('/', isUserAuthenticated, isAdmin, async (req, res) => {
  res.status(200).json({
    success: true,
    data: await ReportsModel.find().populate({ path: 'item', select: 'title' }),
  });
});

ReportsRoutes.get(
  '/item/:itemId',
  isUserAuthenticated,
  isAdmin,
  async (req, res) => {
    res.status(200).json({
      success: true,
      data: await ReportsModel.find({ item: req.params.itemId }),
    });
  },
);
module.exports = ReportsRoutes;
