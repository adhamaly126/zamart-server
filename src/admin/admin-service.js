const MethodNotAllowedException = require('../common/errors/method-not-allowed-exception');
const NotFoundException = require('../common/errors/not-found-exception');
const AdminModel = require('./admin-model');
const bcrypt = require('bcrypt');

class AdminService {
  constructor() {}

  async login(body) {
    const { userName, password } = body;

    const admin = await AdminModel.findOne({ userName: userName })
      .select('+password')
      .exec();

    if (!admin) throw new NotFoundException('Admin username is not exist');

    const isPasswordMatches = await bcrypt.compare(password, admin.password);

    if (!isPasswordMatches)
      throw new MethodNotAllowedException('Invalid admin credentials');

    return admin;
  }

  async injectAdmin() {
    const isExist = await AdminModel.findOne({
      userName: process.env.ADMIN_USERNAME,
    });
    if (isExist) return;

    const hashedPassword = await bcrypt.hash(
      String(process.env.ADMIN_PASSWORD),
      parseInt(process.env.SALT),
    );

    await AdminModel.create({
      userName: process.env.ADMIN_USERNAME,
      password: hashedPassword,
    });
  }
}

module.exports = AdminService;
