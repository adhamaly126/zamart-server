const AdminRoutes = require('express').Router();

const ForbiddenException = require('../common/errors/forbidden-exception');
const JwtService = require('../common/services/jwt-service');
const AdminService = require('./admin-service');
const adminService = new AdminService();
const jwt = require('jsonwebtoken');

AdminRoutes.post('/sign-in', loginController);
AdminRoutes.post('/refresh-token', refreshTokenController);

async function loginController(req, res) {
  const admin = await adminService.login(req.body);

  // Generates Tokens
  const { accessToken, refreshToken } = JwtService.generateTokens({
    id: admin._id,
    role: 'ADMIN',
  });

  res.status(200).json({
    success: true,
    data: { ...admin._doc, accessToken, refreshToken, password: undefined },
  });
}

function refreshTokenController(req, res) {
  const { expiredRefreshToken } = req.body;

  let decodedPayload;
  // Verify
  try {
    decodedPayload = jwt.verify(
      expiredRefreshToken,
      process.env.REFRESH_TOKEN_SECRETS,
    );
  } catch (error) {
    throw new ForbiddenException('Forbidden Access');
  }

  // Generates Tokens
  const { accessToken, refreshToken } = JwtService.generateTokens({
    id: decodedPayload.id,
    role: decodedPayload.role,
  });

  res.status(200).json({
    success: true,
    data: { accessToken, refreshToken },
  });
}

module.exports = AdminRoutes;
