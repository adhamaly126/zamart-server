// Import mongoose
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AdminSchema = new Schema(
  {
    userName: { type: String },
    email: { type: String },
    phone: { type: String },
    password: { type: String, select: false },
  },
  {
    timestamps: true,
  },
);

const AdminModel = mongoose.model('admin', AdminSchema);
module.exports = AdminModel;
