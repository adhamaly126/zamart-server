const ContactUsRoutes = require('express').Router();
const {
  isUserAuthenticated,
  isAdmin,
  isGuestOrAuthenticatedUser,
} = require('../common/middlewares/auth-middlewares');
const ContactUsModel = require('./contact-us-model');

ContactUsRoutes.post('/', isGuestOrAuthenticatedUser, async (req, res) => {
  await ContactUsModel.create(req.body);
  res.status(200).json({
    success: true,
  });
});

ContactUsRoutes.get('/', isUserAuthenticated, isAdmin, async (req, res) => {
  res.status(200).json({
    success: true,
    data: await ContactUsModel.find(),
  });
});

ContactUsRoutes.get(
  '/:contactUsId',
  isUserAuthenticated,
  isAdmin,
  async (req, res) => {
    res.status(200).json({
      success: true,
      data: await ContactUsModel.findById(req.params.contactUsId),
    });
  },
);
module.exports = ContactUsRoutes;
