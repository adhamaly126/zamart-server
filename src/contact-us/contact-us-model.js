const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ContactUsSchema = new Schema(
  {
    firstName: { type: String },
    familyName: { type: String },
    email: { type: String },
    subject: { type: String },
    message: { type: String },
  },
  { timestamps: true },
);

const ContactUsModel = mongoose.model('contact-us-request', ContactUsSchema);
module.exports = ContactUsModel;
