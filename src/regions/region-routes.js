const regionsRoutes = require('express').Router();

const {
  isGuestOrAuthenticatedUser,
  isUserAuthenticated,
  isAdmin,
} = require('../common/middlewares/auth-middlewares');
const RegionsSerivce = require('./region-service');
const regionsSerivce = new RegionsSerivce();

regionsRoutes.post(
  '/countries',
  isUserAuthenticated,
  isAdmin,
  async (req, res) => {
    await regionsSerivce.createCountry(req.body);
    res.status(201).json({
      success: true,
    });
  },
);

regionsRoutes.post(
  '/countries/cities',
  isUserAuthenticated,
  isAdmin,
  async (req, res) => {
    await regionsSerivce.createCitiesForCountry(req.body);
    res.status(201).json({
      success: true,
    });
  },
);

regionsRoutes.get(
  '/countries/all',
  isGuestOrAuthenticatedUser,
  findAllCountries,
);
regionsRoutes.get('/cities', isGuestOrAuthenticatedUser, findAllCities);

async function findAllCountries(req, res) {
  res.status(200).json({
    success: true,
    data: await regionsSerivce.findAllCountries(),
  });
}
async function findAllCities(req, res) {
  const { country } = req.query;

  res.status(200).json({
    success: true,
    data: await regionsSerivce.findCities(country),
  });
}
module.exports = regionsRoutes;
