const CityModel = require('./models/city-model');
const CountryModel = require('./models/country-model');

class RegionsSerivce {
  constructor() {}
  async findAllCountries() {
    return await CountryModel.find().exec();
  }

  async createCountry(data) {
    return await CountryModel.create({ ...data });
  }

  async createCitiesForCountry(countryCities) {
    countryCities.cities?.forEach(async (cityData) => {
      return await CityModel.create({
        ...cityData,
        country: countryCities.country,
      });
    });
  }

  async findCities(country) {
    return await CityModel.find({
      ...(country ? { country: country } : {}),
    }).exec();
  }

  async findCountryById(id) {
    return await CountryModel.findById(id).exec();
  }
}

module.exports = RegionsSerivce;
