const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CountrySchema = new Schema(
  {
    nameAr: { type: String },
    nameEn: { type: String },
    currency: { type: String },
  },
  { timestamps: true },
);
const CountryModel = mongoose.model('country', CountrySchema);
module.exports = CountryModel;
