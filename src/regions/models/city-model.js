const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CitySchema = new Schema(
  {
    nameAr: { type: String },
    nameEn: { type: String },
    country: { type: Schema.Types.ObjectId, ref: 'country' },
  },
  { timestamps: true },
);
const CityModel = mongoose.model('city', CitySchema);
module.exports = CityModel;
