const AbstractError = require("./abstract-error");

exports.RequestValidationError = class RequestValidationError extends AbstractError {
  constructor(errors) {
    super(errors);
    this.errors = errors;
    this.name = "RequestValidationError";
    this.statusCode = 400;
  }

  serializeErrors() {
    return this.errors.map((e) => {
      if (e.nestedErrors)
        return e.nestedErrors.map((nestedError) => ({
          message: nestedError.msg,
          ...(nestedError.param && { field: nestedError.param })
        }));
      else return { message: e.msg, ...(e.param && { field: e.param }) };
    });
  }
};
