const { validationResult } = require("express-validator");
const fs = require("fs");

const MethodNotAllowed = require("../errors/method-not-allowed-exception");

exports.handleRouteValidationError = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    if (req.file) {
      // eslint-disable-next-line security/detect-non-literal-fs-filename
      fs.unlink(`${process.cwd()}/${req.file.path}`, (err) => (err ? console.error(err) : null));
    }

    throw new MethodNotAllowed(errors.array());
  } else {
    next();
  }
};
