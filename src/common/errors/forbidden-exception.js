const AbstractError = require("./abstract-error");

/**
 * @example throw new Forbidden('There is no user with this id')
 */
class ForbiddenException extends AbstractError {
  constructor(message = "Forbidden") {
    super(message);
    this.name = "Forbidden";
    this.statusCode = 403;
  }
}

module.exports = ForbiddenException;
