const AbstractError = require("./abstract-error");

/**
 * @example throw new Unauthorized('There is no user with this id')
 */
class UnauthorizedException extends AbstractError {
  constructor(message = "Unauthorized") {
    super(message);
    this.name = "Unauthorized";
    this.statusCode = 401;
  }
}

module.exports = UnauthorizedException;
