const jwt = require('jsonwebtoken');
const MethodNotAllowedException = require('../errors/method-not-allowed-exception');
const ForbiddenException = require('../errors/forbidden-exception');
const UnauthorizedException = require('../errors/unauthorized-exception');

function isUserAuthenticated(req, res, next) {
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    token = req.headers.authorization.split(' ')[1];
  }

  try {
    //reload payload from token into decoded
    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRETS);
    if (!decoded) throw new MethodNotAllowedException('Invalid Authentication');

    // adding payload {id,role} into request.user object
    req.user = decoded;
    // eslint-disable-next-line no-console
    console.log('user -> ', req.user);
  } catch (err) {
    throw new ForbiddenException('Forbidden Access');
  }
  return next();
}

function isAdmin(req, res, next) {
  if (req.user.role === 'ADMIN') return next();

  throw new UnauthorizedException('You are not authorized for this operation');
}

function isGuestOrAuthenticatedUser(req, res, next) {
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    token = req.headers.authorization.split(' ')[1];
  } else {
    req.user = { role: 'guest' };
    console.log('Guest ->', req.user);
    return next();
  }

  try {
    //reload payload from token into decoded
    const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRETS);
    if (!decoded) throw new MethodNotAllowedException('Invalid Authentication');

    // adding payload {id,role} into request.user object
    req.user = decoded;
    // eslint-disable-next-line no-console
    console.log('user -> ', req.user);
  } catch (err) {
    throw new ForbiddenException('Forbidden Access');
  }
  return next();
}

module.exports = { isUserAuthenticated, isGuestOrAuthenticatedUser, isAdmin };
