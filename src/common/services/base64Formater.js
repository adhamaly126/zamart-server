class Base64Formater {
  constructor() {}

  static parseToBase64(binaryString) {
    return Buffer.from(binaryString, 'binary').toString('base64');
  }
}
module.exports = Base64Formater;
