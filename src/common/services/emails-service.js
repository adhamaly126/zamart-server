const nodemailer = require('nodemailer');

class EmailService {
  constructor() {}

  static sendEmail(email, token, mailType) {
    const transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      secure: true,
      port: 465,
      auth: {
        user: process.env.EMAIL_FROM,
        pass: process.env.EMAIL_PASSWORD,
      },
    });

    let mailOptions;
    if (mailType === 'VERIFICATION') {
      mailOptions = {
        from: {
          name: 'Zamart Team',
          address: process.env.EMAIL_FROM,
        },
        to: email,
        subject: `Zamart Email Verification`,
        html: `<h1>Please use the activation link to activate your account </h1>
          <p>${process.env.CLIENT_URL}/user/activate?token=${token}</p>`,
      };
    } else
      mailOptions = {
        from: {
          name: 'Zamart Team',
          address: process.env.EMAIL_FROM,
        },
        to: email,
        subject: `Zamart Reset Password Confirmation`,
        html: `<h1>Please use the link to reset your password </h1>
        <p>${process.env.FRONT_URL}/auth/zamart/reset-password?token=${token}</p>`,
      };
    try {
      transporter.sendMail(mailOptions, (err, info) => {
        if (err) console.log(err);
        else console.log('EMAIL SENT SUCCESS');
      });
    } catch (error) {}
  }
}

module.exports = EmailService;
