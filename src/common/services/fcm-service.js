const { firebaseAdmin } = require('./firebase-config');

class FCMService {
  constructor() {}

  async subscribeToTopic(registrationTokens, topic) {
    try {
      await firebaseAdmin
        .messaging()
        .subscribeToTopic(registrationTokens, topic);
    } catch (error) {
      console.log(error);
      console.log(error.errors[0].error);
    }
  }

  async unSubscribeToTopic(registrationTokens, topic) {
    try {
      await firebaseAdmin
        .messaging()
        .unsubscribeFromTopic(registrationTokens, topic);
    } catch (error) {
      console.log(error);
    }
  }

  async sendNotificationToTopic(message) {
    try {
      await firebaseAdmin.messaging().send(message);
    } catch (error) {
      console.log(error);
    }
  }

  static async pushNotificationToDevice(message) {
    if (message.token) {
      try {
        // push message to user token
        await firebaseAdmin.messaging().send(message);
      } catch (error) {
        console.log(error);
      }
    }
  }
}

module.exports = FCMService;
