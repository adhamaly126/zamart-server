const jwt = require('jsonwebtoken');
const ForbiddenException = require('../errors/forbidden-exception');

class JwtService {
  constructor() {}

  static generateTokens(payload) {
    const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRETS, {
      expiresIn: '15m',
    });
    const refreshToken = jwt.sign(payload, process.env.REFRESH_TOKEN_SECRETS, {
      expiresIn: '7d',
    });

    return { accessToken, refreshToken };
  }
}

module.exports = JwtService;
