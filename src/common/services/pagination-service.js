class PaginationService {
  constructor() {}

  static getSkipAndLimit(page, perPage) {
    const skip = (page - 1) * perPage;
    const limit = perPage;
    return { skip, limit };
  }
  static getTotalPages(documentsCount, limit) {
    const totalPages = documentsCount / limit;
    return Math.ceil(totalPages);
  }
}

module.exports = PaginationService;
