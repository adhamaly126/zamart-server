const MethodNotAllowedException = require('../common/errors/method-not-allowed-exception');
const { ItemStatus } = require('./enums/item-status');

class ItemStatusValidator {
  constructor() {}
  StatusHirarchy = {
    '': [ItemStatus.DRAFTED, ItemStatus.PENDING],
    [ItemStatus.DRAFTED]: [ItemStatus.PENDING],
    [ItemStatus.PENDING]: [ItemStatus.PUBLISHED, ItemStatus.REJECTED],
    [ItemStatus.PUBLISHED]: [
      ItemStatus.SOLD,
      ItemStatus.EXPIRED,
      ItemStatus.ARCHIVED,
    ],
    [ItemStatus.REJECTED]: [],
    [ItemStatus.ARCHIVED]: [],
  };

  isStatusValidForItem(item, newStatus) {
    if (!this.StatusHirarchy[item.status].includes(newStatus))
      throw new MethodNotAllowedException('حالة السلعة غير صالحة ');
  }
}
module.exports = ItemStatusValidator;
