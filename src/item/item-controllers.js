const sharp = require('sharp');
const ItemService = require('./item-service');
const itemService = new ItemService();
const fs = require('fs/promises');
const path = require('path');
const MethodNotAllowedException = require('../common/errors/method-not-allowed-exception');

async function createItem(req, res) {
  if (!req.files?.length)
    throw new MethodNotAllowedException('Upload At Least Three Images');

  const bufferedImages = await _filesSharping(req.files);
  res.status(201).json({
    success: true,
    data: await itemService.createNewItem(
      req.user.id,
      req.body,
      bufferedImages,
    ),
  });
}

async function _filesSharping(files) {
  let bufferedImages = [];
  console.log('files = ', files);

  for (let i = 0; i < files.length; i++) {
    const bufferedImage = await sharp(files[i].path)
      .resize({
        width: 97,
        height: 97,
      })
      .webp({ lossless: true })
      .toBuffer();

    bufferedImages.push(bufferedImage);
    await fs.unlink(process.cwd() + '/uploads/' + files[i].filename);
  }

  return bufferedImages;
}
async function viewAllItems(req, res) {
  const result = await itemService.findAllForView(req.query);
  res.status(200).json({
    success: true,
    totalPages: result.totalPages,
    totalItems: result.totalItems,
    data: result.items,
  });
}
async function viewAllItemsForOwner(req, res) {
  const result = await itemService.findAllForOwner(req.user.id, req.query);
  res.status(200).json({
    success: true,
    totalPages: result.totalPages,
    totalItems: result.totalItems,
    data: result.items,
  });
}
async function viewAllItemsAnalyticsForOwner(req, res) {
  res.status(200).json({
    success: true,
    data: await itemService.findItemsAnalyticsForOwner(req.user.id),
  });
}
async function publishItemByOwner(req, res) {
  await itemService.itemPublishingByOwner(req.params.itemId, req.user.id);
  res.status(200).json({
    success: true,
  });
}
async function sendEnquiryForItem(req, res) {
  await itemService.sendEnquiryToOwner(req.params.itemId, req.body);
  res.status(200).json({
    success: true,
  });
}
async function viewAllEnquiriesForItem(req, res) {
  res.status(200).json({
    success: true,
    data: await itemService.findAllItemEnquiriesForOwner(
      req.user.id,
      req.params.itemId,
    ),
  });
}
async function sendMakeCall(req, res) {
  res.status(200).json({
    success: true,
    data: await itemService.makeCallNowRequest(req.user.id, req.params.itemId),
  });
}
async function viewAllItemsForAdmin(req, res) {
  const result = await itemService.findAllForAdmin(req.query);
  res.status(200).json({
    success: true,
    totalPages: result.totalPages,
    totalItems: result.totalItems,
    data: result.items,
  });
}
async function viewItemById(req, res) {
  res.status(200).json({
    success: true,
    data: await itemService.findItemById(req.params.itemId),
  });
}
async function publishItemByAdmin(req, res) {
  await itemService.itemApprovalFromAdmin(req.params.itemId);
  res.status(200).json({
    success: true,
  });
}
async function rejectItemByAdmin(req, res) {
  await itemService.itemRejectionFromAdmin(req.params.itemId);
  res.status(200).json({
    success: true,
  });
}

async function archiveItemByOwnerController(req, res) {
  await itemService.archiveItemByOwner(req.user.id, req.params.itemId);
  res.status(200).json({
    success: true,
  });
}

module.exports = {
  createItem,
  viewItemById,
  viewAllItems,
  viewAllItemsForOwner,
  viewAllItemsForAdmin,
  publishItemByAdmin,
  rejectItemByAdmin,
  publishItemByOwner,
  sendEnquiryForItem,
  viewAllEnquiriesForItem,
  sendMakeCall,
  archiveItemByOwnerController,
  viewAllItemsAnalyticsForOwner,
};
