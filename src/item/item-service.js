const mongoose = require('mongoose');
const PaginationService = require('../common/services/pagination-service');
const ItemsModel = require('./models/item-model');
const { ItemStatus } = require('./enums/item-status');
const UnauthorizedException = require('../common/errors/unauthorized-exception');
const ItemStatusValidator = require('./item-status-validator');
const itemStatusValidator = new ItemStatusValidator();
const ItemEnquiriesModel = require('./models/enquiry-model');
const ItemCallModel = require('./models/item-calls-models');
const MethodNotAllowedException = require('../common/errors/method-not-allowed-exception');
const ImagesModel = require('./models/image-model');
const fs = require('fs');
const path = require('path');
const UserModel = require('../users/user-model');
const NotificationService = require('../notifications/notifications-service');
const AdminModel = require('../admin/admin-model');

class ItemService {
  constructor() {
    this.notificationService = new NotificationService();
  }

  itemViewPopulations = [
    { path: 'user', select: 'fullName imageLink imagePath' },
    { path: 'category', select: 'nameEn nameAr' },
    { path: 'subCategory', select: 'nameEn nameAr' },
    { path: 'complementaryCategory', select: 'nameEn nameAr' },
    { path: 'city', select: 'nameAr nameEn' },
  ];

  async createNewItem(ownerId, data, images) {
    const createdItem = await ItemsModel.create({
      user: ownerId,
      ...data,
    });

    await this._insertItemImages(createdItem._id, images);

    if (createdItem.status === ItemStatus.PENDING) {
      const admin = await AdminModel.findOne();
      await this.notificationService.sendOwnerAddNewItemNotification(
        createdItem._id,
        admin._id,
      );
    }

    return createdItem;
  }

  async _insertItemImages(itemId, images) {
    console.log(images);

    for (let i = 0; i < images.length; i++) {
      await ImagesModel.create({
        img: images[i],
        item: itemId,
      });
    }
  }

  async findAllForView(query) {
    const { skip, limit } = PaginationService.getSkipAndLimit(
      parseInt(query.page) || 1,
      parseInt(query.perPage) || 10,
    );

    const filter = this._applyFilter(query);

    const items = await ItemsModel.aggregate([
      {
        $match: { ...filter, status: ItemStatus.PUBLISHED },
      },
      {
        $lookup: {
          from: 'images',
          localField: '_id',
          foreignField: 'item',
          as: 'images',
        },
      },
      { $skip: skip },
      { $limit: limit },
    ]).exec();

    await ItemsModel.populate(items, this.itemViewPopulations);

    // if (Object.keys(filter).includes('title')) {
    //   items.forEach(async (item) => {
    //     await ItemsModel.findByIdAndUpdate(item._id, {
    //       $inc: { totalSearch: 1 },
    //     });
    //   });
    // }
    const count = await ItemsModel.countDocuments({
      ...filter,
      status: ItemStatus.PUBLISHED,
    });

    return {
      items: items,
      totalPages: PaginationService.getTotalPages(count, limit),
      totalItems: count,
    };
  }

  async findAllForOwner(ownerId, query) {
    const { page = 1, perPage = 10, status } = query;

    const { skip, limit } = PaginationService.getSkipAndLimit(
      parseInt(page),
      parseInt(perPage),
    );

    const items = await ItemsModel.aggregate([
      {
        $match: {
          user: mongoose.Types.ObjectId(ownerId),
          ...(status ? { status: status } : {}),
        },
      },
      {
        $lookup: {
          from: 'images',
          localField: '_id',
          foreignField: 'item',
          as: 'images',
        },
      },

      { $skip: skip },
      { $limit: limit },
    ]).exec();

    await ItemsModel.populate(items, this.itemViewPopulations);

    const count = await ItemsModel.countDocuments({
      user: mongoose.Types.ObjectId(ownerId),
      ...(status ? { status: status } : {}),
    });

    return {
      items: items,
      totalPages: PaginationService.getTotalPages(count, limit),
      totalItems: count,
    };
  }

  async findAllForAdmin(query) {
    const { page = 1, perPage = 10 } = query;

    const { skip, limit } = PaginationService.getSkipAndLimit(
      parseInt(page),
      parseInt(perPage),
    );

    const filter = this._applyFilter(query);

    const items = await ItemsModel.aggregate(
      [
        {
          $match: { ...filter, status: ItemStatus.PENDING },
        },
        {
          $lookup: {
            from: 'images',
            localField: '_id',
            foreignField: 'item',
            as: 'images',
          },
        },
        { $skip: skip },
        { $limit: limit },
      ],
      { allowDiskUse: true },
    ).exec();

    await ItemsModel.populate(items, this.itemViewPopulations);

    const count = await ItemsModel.countDocuments({
      ...filter,
      status: ItemStatus.PENDING,
    });

    return {
      items: items,
      totalPages: PaginationService.getTotalPages(count, limit),
      totalItems: count,
    };
  }

  async findItemsAnalyticsForOwner(ownerId) {
    return await ItemsModel.aggregate([
      {
        $match: { user: mongoose.Types.ObjectId(ownerId) },
      },
      {
        $group: {
          _id: '$status',
          count: { $sum: 1 },
        },
      },
      {
        $project: {
          _id: 0,
          status: '$_id',
          count: 1,
          sum: 1,
        },
      },
    ]).exec();
  }

  async itemApprovalFromAdmin(itemId) {
    const item = await ItemsModel.findById(itemId).exec();

    itemStatusValidator.isStatusValidForItem(item, ItemStatus.PUBLISHED);

    const ownerOfItem = await this._getItemOwner(itemId);

    const expiryDate = this.calculateExpiryDate();
    await ItemsModel.findByIdAndUpdate(itemId, {
      $set: {
        status: ItemStatus.PUBLISHED,
        expiryDate: expiryDate,
      },
    }).exec();

    await this.notificationService.itemPublishedByAdminNotification(
      ownerOfItem._id,
      itemId,
      ownerOfItem.langugae,
      ownerOfItem.fcmToken,
    );
  }
  calculateExpiryDate() {
    const currentDate = new Date();
    return new Date(
      currentDate.setDate(
        currentDate.getDate() + parseInt(process.env.EXPIRY_DURATION),
      ),
    );
  }
  async itemRejectionFromAdmin(itemId) {
    const item = await ItemsModel.findById(itemId).exec();

    itemStatusValidator.isStatusValidForItem(item, ItemStatus.REJECTED);

    item.status = ItemStatus.REJECTED;
    await item.save();

    const ownerOfItem = await this._getItemOwner(itemId);

    await this.notificationService.itemRejectedByAdminNotification(
      ownerOfItem._id,
      itemId,
      ownerOfItem.langugae,
      ownerOfItem.fcmToken,
    );
  }

  async itemPublishingByOwner(itemId, ownerId) {
    await this._isOwnerOfItem(ownerId, itemId);

    const item = await ItemsModel.findById(itemId).exec();

    itemStatusValidator.isStatusValidForItem(item, ItemStatus.PENDING);

    item.status = ItemStatus.PENDING;
    await item.save();
  }

  async archiveItemByOwner(ownerId, itemId) {
    await this._isOwnerOfItem(ownerId, itemId);
    const item = await ItemsModel.findById(itemId).exec();

    itemStatusValidator.isStatusValidForItem(item, ItemStatus.ARCHIVED);

    item.status = ItemStatus.ARCHIVED;
    await item.save();
  }

  async findItemById(id) {
    const item = await ItemsModel.aggregate([
      {
        $match: { _id: mongoose.Types.ObjectId(id) },
      },
      {
        $lookup: {
          from: 'images',
          localField: '_id',
          foreignField: 'item',
          as: 'images',
        },
      },
    ]).exec();

    await ItemsModel.populate(item[0], this.itemViewPopulations);

    await ItemsModel.findByIdAndUpdate(id, { $inc: { totalViews: 1 } });

    return { ...item[0] };
  }

  async sendEnquiryToOwner(itemId, enquiryData) {
    await ItemEnquiriesModel.create({ item: itemId, ...enquiryData });
    await ItemsModel.findByIdAndUpdate(itemId, { $inc: { totalEnquires: 1 } });
  }

  async findAllItemEnquiriesForOwner(ownerId, itemId) {
    await this._isOwnerOfItem(ownerId, itemId);

    return await ItemEnquiriesModel.find({ item: itemId }).exec();
  }

  async makeCallNowRequest(userId, itemId) {
    const itemOwner = await this._getItemOwner(itemId);

    // Check Calls exists
    const userCalls = await ItemCallModel.findOne({
      user: userId,
      item: itemId,
    }).exec();

    await ItemsModel.findByIdAndUpdate(itemId, { $inc: { totalCalls: 1 } });

    if (userCalls) {
      // Check calls count
      if (userCalls.count >= 9)
        throw new MethodNotAllowedException(
          'لقد تخطيت عدد المكالمات لهذه السلعة',
        );

      userCalls.count += 1;
      await userCalls.save();

      return itemOwner.phone;
    }

    await ItemCallModel.create({ user: userId, item: itemId });
    return itemOwner.phone;
  }

  // Helper fucntions
  _applyFilter(queryParamters) {
    const {
      category,
      priceFrom,
      priceTo,
      subCategory,
      title,
      model,
      brand,
      transimision,
      year,
      usage,
      city,
    } = queryParamters;

    let filter = {};
    if (category) {
      filter = {
        ...filter,
        ...{ category: mongoose.Types.ObjectId(category) },
      };
    }
    if (subCategory) {
      filter = {
        ...filter,
        ...{ subCategory: mongoose.Types.ObjectId(subCategory) },
      };
    }
    if (priceFrom && priceTo) {
      filter = {
        ...filter,
        ...{
          $and: [
            { price: { $gte: parseInt(priceFrom) } },
            { price: { $lte: parseInt(priceTo) } },
          ],
        },
      };
    }
    if (title) {
      filter = {
        ...filter,
        ...{ title: { $regex: '.*' + title + '.*' } },
      };
    }
    if (model) {
      filter = {
        ...filter,
        ...{ model: model },
      };
    }
    if (brand) {
      filter = {
        ...filter,
        ...{ brand: brand },
      };
    }
    if (transimision) {
      filter = {
        ...filter,
        ...{ transimision: transimision },
      };
    }
    if (year) {
      filter = {
        ...filter,
        ...{ year: year },
      };
    }
    if (usage) {
      filter = {
        ...filter,
        ...{ usage: usage },
      };
    }
    if (city) {
      filter = {
        ...filter,
        ...{ city: mongoose.Types.ObjectId(city) },
      };
    }
    return filter;
  }
  async _isOwnerOfItem(ownerId, itemId) {
    const item = await ItemsModel.findById(itemId).exec();
    if (item.user.toString() != ownerId.toString())
      throw new UnauthorizedException('غير مصرح لك');
  }

  async _getItemOwner(itemId) {
    const item = await ItemsModel.findById(itemId).exec();
    return await UserModel.findOne({ _id: item.user, isDeleted: false });
  }

  async checkExpiryDateForExpiredItems() {
    await ItemsModel.updateMany(
      {
        status: ItemStatus.PUBLISHED,
        expiryDate: { $lte: new Date() },
      },
      {
        $set: { status: ItemStatus.EXPIRED },
      },
    ).exec();
  }
}

module.exports = ItemService;
