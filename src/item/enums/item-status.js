const ItemStatus = {
  DRAFTED: 'DRAFTED',
  PENDING: 'PENDING',
  PUBLISHED: 'PUBLISHED',
  SOLD: 'SOLD',
  REJECTED: 'REJECTED',
  EXPIRED: 'EXPIRED',
  ARCHIVED: 'ARCHIVED',
};

module.exports = { ItemStatus };
