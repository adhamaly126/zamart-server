const { body } = require('express-validator');
const {
  handleRouteValidationError,
} = require('../common/errors/handle-validation-error');

const itemCreationValidation = [
  body('title')
    .notEmpty()
    .withMessage('Enter title')
    .bail()
    .isString()
    .withMessage('Enter Valid Title'),
  body('description')
    .notEmpty()
    .withMessage('Enter Description')
    .bail()
    .isString()
    .withMessage('Enter Valid Description'),
  body('price').notEmpty().withMessage('Enter Price'),
  body('usage').notEmpty().withMessage('Enter Usage'),
  body('brand')
    .notEmpty()
    .withMessage('Enter Brand')
    .bail()
    .isString()
    .withMessage('Enter Valid Brand'),
  body('model')
    .notEmpty()
    .withMessage('Enter Model')
    .bail()
    .isString()
    .withMessage('Enter Valid Model'),
  body('city')
    .notEmpty()
    .withMessage('Enter City')
    .bail()
    .isMongoId()
    .withMessage('Enter Valid City'),
  body('category')
    .notEmpty()
    .withMessage('Enter Category')
    .bail()
    .isMongoId()
    .withMessage('Enter Valid Category'),
  body('subCategory').if(
    body('subCategory')
      .exists()
      .notEmpty()
      .withMessage('Enter Sub-Category')
      .bail()
      .isMongoId()
      .withMessage('Enter Valid Sub-Category'),
  ),
  body('complementaryCategory').if(
    body('complementaryCategory')
      .exists()
      .notEmpty()
      .withMessage('Enter Complementary Category')
      .bail()
      .isMongoId()
      .withMessage('Enter Valid Complementary Category'),

    body('status')
      .notEmpty()
      .withMessage('Decide Status For Item')
      .bail()
      .isIn(['PENDING', 'DRAFTED'])
      .withMessage('Enter Valid Status'),
  ),
  handleRouteValidationError,
];

module.exports = { itemCreationValidation };
