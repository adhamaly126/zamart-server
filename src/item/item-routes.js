const ItemsRoutes = require('express').Router();
const uuid4 = require('uuid');
const multer = require('multer');

const {
  isUserAuthenticated,
  isGuestOrAuthenticatedUser,
  isAdmin,
} = require('../common/middlewares/auth-middlewares');
const {
  createItem,
  viewItemById,
  viewAllItems,
  viewAllItemsForOwner,
  viewAllItemsForAdmin,
  publishItemByAdmin,
  rejectItemByAdmin,
  publishItemByOwner,
  sendEnquiryForItem,
  sendMakeCall,
  viewAllEnquiriesForItem,
  archiveItemByOwnerController,
  viewAllItemsAnalyticsForOwner,
} = require('./item-controllers');
const { itemCreationValidation } = require('./item-validation');
const MethodNotAllowedException = require('../common/errors/method-not-allowed-exception');

const storage = multer.diskStorage({
  destination: `uploads`,
  filename(req, file, cb) {
    cb(
      null,
      file.fieldname +
        '-' +
        file.originalname +
        '-' +
        Date.now() +
        '' +
        new Date().getMilliseconds(),
    );
  },
  filetype(req, file, cb) {
    cb(null, path.extname(file.originalname));
  },
});

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 10 * 1024 * 1024,
  },
});

const uploadFiles = upload.array('images', 5); // limit to 5 images
const uploadImages = (req, res, next) => {
  uploadFiles(req, res, (err) => {
    if (err instanceof multer.MulterError) {
      // A Multer error occurred when uploading.
      if (err.code === 'LIMIT_UNEXPECTED_FILE') {
        // Too many images exceeding the allowed limit
        return res.status(405).json({
          message: 'Too many images exceeding the allowed limit',
        });
        // ...
      } else if (err.code === 'LIMIT_FILE_SIZE') {
        return res.status(405).json({
          message: 'File size is too large',
        });
      } else {
        console.log(err);
        return res.status(405).json({
          message: 'Failed while uploading images',
        });
      }
    }

    // Everything is ok.
    next();
  });
};

// Create Item
ItemsRoutes.post(
  '',
  isUserAuthenticated,
  uploadImages,
  itemCreationValidation,
  createItem,
);
// View itemById
ItemsRoutes.get('/:itemId/details', viewItemById);
// User View All
ItemsRoutes.get('/user/all', isGuestOrAuthenticatedUser, viewAllItems);
// User"Owner" view All
ItemsRoutes.get('/user/owns-items', isUserAuthenticated, viewAllItemsForOwner);
ItemsRoutes.get(
  '/user/owns-items/analytics',
  isUserAuthenticated,
  viewAllItemsAnalyticsForOwner,
);

// User"Owner" publish item -->PENDIND
ItemsRoutes.patch(
  '/user/publish-item/:itemId',
  isUserAuthenticated,
  publishItemByOwner,
);
// User"Owner" archive item
ItemsRoutes.patch(
  '/user/archive-item/:itemId',
  isUserAuthenticated,
  archiveItemByOwnerController,
);
// User send enquiry for item
ItemsRoutes.post(
  '/user/:itemId/send-enquiry',
  isUserAuthenticated,
  sendEnquiryForItem,
);
// User make call for item
ItemsRoutes.post('/user/:itemId/make-call', isUserAuthenticated, sendMakeCall);
// User "Owner" view all enquires of item
ItemsRoutes.get(
  '/user/:itemId/enquiries',
  isUserAuthenticated,
  viewAllEnquiriesForItem,
);
// Admin view All
ItemsRoutes.get(
  '/admin/all',
  isUserAuthenticated,
  isAdmin,
  viewAllItemsForAdmin,
);
// Admin publish item -->PUBLISHED
ItemsRoutes.patch(
  '/admin/publish-item/:itemId',
  isUserAuthenticated,
  isAdmin,
  publishItemByAdmin,
);
// Admin reject item -->REJECTED
ItemsRoutes.patch(
  '/admin/reject-item/:itemId',
  isUserAuthenticated,
  isAdmin,
  rejectItemByAdmin,
);

module.exports = ItemsRoutes;
