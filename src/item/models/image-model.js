const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ImagesSchema = new Schema({
  item: { type: Schema.Types.ObjectId, ref: 'item' },
  isMain: { type: Boolean, default: false },
  img: { type: Buffer },
});

const ImagesModel = mongoose.model('image', ImagesSchema);
module.exports = ImagesModel;
