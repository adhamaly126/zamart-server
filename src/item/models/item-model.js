const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ItemSchema = new Schema(
  {
    title: { type: String },
    price: { type: Number },
    description: { type: String },
    usage: { type: String },
    kiloMeters: { type: Number },
    year: { type: String },
    bodyType: { type: String },
    horsePower: { type: String },
    bodyCondtion: { type: String },
    doors: { type: String },
    sellerType: { type: String },
    warranty: { type: String },
    fuelType: { type: String },
    finalDriveSystem: { type: String },
    numOfCylinders: { type: Number },
    transimision: { type: String },
    wheels: { type: String },
    brand: { type: String },
    model: { type: String },
    status: { type: String },
    expiryDate: { type: Date },
    city: { type: Schema.Types.ObjectId, ref: 'city' },
    user: { type: Schema.Types.ObjectId, ref: 'user' },
    category: { type: Schema.Types.ObjectId, ref: 'category' },
    subCategory: { type: Schema.Types.ObjectId, ref: 'sub-category' },
    complementaryCategory: {
      type: Schema.Types.ObjectId,
      ref: 'complementary-category',
    },
    totalViews: { type: Number, default: 0 },
    totalCalls: { type: Number, default: 0 },
    totalEnquires: { type: Number, default: 0 },
    totalSearch: { type: Number, default: 0 },
  },
  {
    timestamps: true,
  },
);

const ItemsModel = mongoose.model('item', ItemSchema);
module.exports = ItemsModel;
