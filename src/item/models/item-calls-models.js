const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ItemCallSchema = new Schema(
  {
    item: { type: Schema.Types.ObjectId, ref: 'item' },
    user: { type: Schema.Types.ObjectId, ref: 'user' },
    count: { type: Number, default: 1 },
  },
  { timestamps: true },
);

const ItemCallModel = mongoose.model('item-calls', ItemCallSchema);
module.exports = ItemCallModel;
