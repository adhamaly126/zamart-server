const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ItemEnquiriesSchema = new Schema(
  {
    item: { type: Schema.Types.ObjectId, ref: 'item' },
    description: { type: String },
    enquiryUserFullName: { type: String },
    enquiryUserEmail: { type: String },
    enquiryUserPhone: { type: String },
    reason: { type: String },
  },
  { timestamps: true },
);

const ItemEnquiriesModel = mongoose.model('item-enquiry', ItemEnquiriesSchema);
module.exports = ItemEnquiriesModel;
