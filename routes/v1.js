const AppRoutes = require('express').Router();

const AdminRoutes = require('../src/admin/admin-routes');
const BrandsRoutes = require('../src/brands/brands-routes');
const categoriesRoutes = require('../src/categories/categories.routes');
const ContactUsRoutes = require('../src/contact-us/contact-us-routes');
const ItemsRoutes = require('../src/item/item-routes');
const notificationsRoutes = require('../src/notifications/notifications-routes');
const regionsRoutes = require('../src/regions/region-routes');
const ReportsRoutes = require('../src/reports/reports-routes');
const userRoutes = require('../src/users/user-routes');

AppRoutes.use('/items', ItemsRoutes);
AppRoutes.use('/user', userRoutes);
AppRoutes.use('/admins', AdminRoutes);
AppRoutes.use('/categories', categoriesRoutes);
AppRoutes.use('/regions', regionsRoutes);
AppRoutes.use('/brands', BrandsRoutes);
AppRoutes.use('/reports', ReportsRoutes);
AppRoutes.use('/contact-us', ContactUsRoutes);
AppRoutes.use('/notifications', notificationsRoutes);

module.exports = AppRoutes;
